const express = require('express')
const app = express()
const port = 5500

app.use(express.static('public'))
app.get('/hello', (req, res) => {
    console.log('first')
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port -  ${port}`)
})